package controller;

import java.io.File;

import api.ISTSMAnager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.RedBlackBST;
import model.data_structures.TablaHash;
import model.exceptions.DateNotFoundException;
import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import model.vo.AgencyVO;
import model.vo.StopVO;
import model.vo.VOTransfer;
import model.vo.TripVO;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VORetardo;
import model.vo.VORoute;
import model.vo.VOStopTimes;
import model.vo.VOTransfer;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSMAnager  manager = new STSManager();
	
	private static String fecha;
	
	public static void buscarLat(){
		manager.buscarLat();
	}
	
	public static void readStopsEstimService(){
		File f = new File("./data3/RealTime-8-21-STOPS_ESTIM_SERVICES/RealTime-8-21-STOPS_ESTIM_SERVICES");
		File[] updateFiles = f.listFiles();
		//System.out.println(updateFiles.length);
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readStopsEstimService(updateFiles[i]);
		}
	}

	public static void readStopsEstimService2(){
		File f = new File("./data5/RealTime-8-22-STOPS_ESTIM_SERVICES");
		File[] updateFiles = f.listFiles();
		//System.out.println(updateFiles.length);
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readStopsEstimService(updateFiles[i]);
		}
	}
	public static void readBusUpdates() {
		File f = new File("data");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	
	public static void readBusUpdates2() {
		File f = new File("./data4/RealTime-8-22-BUSES_SERVICE/RealTime-8-22-BUSES_SERVICE");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	
	
	public static void loadFiles(){
		File f= new File("data2");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			if (updateFiles[i].getName().startsWith("agency.txt")) {
				manager.loadAgencies(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("stops.txt")) {
				manager.loadStops(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("calendar_dates")) {
				manager.loadCalendarDates(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("calendar.txt")) {
				manager.loadCalendar(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("feed")) {
				manager.loadFeedInfo(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("routes")) {
				manager.loadRoutes(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("shapes")) {
				manager.loadShapes(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("stop_times")) {
				manager.loadStopTimes(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("transfers")) {
				manager.loadTransfers(updateFiles[i].getPath());
			}
			else if (updateFiles[i].getName().startsWith("trips")) {
				manager.loadTrips(updateFiles[i].getPath());
			}
		}
	}
	
	
	public static void ITScargarGTFS() 
	{
		loadFiles();
	}

	public static void ITScargarTR(String Pfecha) throws DateNotFoundException
	{
		if (Pfecha.equals("20170822")) {
			fecha = Pfecha;
			readStopsEstimService2();
			readBusUpdates2();
			manager.cargarRetardosViaje();
		}
		else if (Pfecha.equals("20170821")) {
			fecha = Pfecha;
			readStopsEstimService();
			readBusUpdates();
			manager.cargarRetardosViaje();
		}
		else {
			System.out.println("No hay datos para esta fecha");
		}
	}
	public static void tieneCiclos(){
		manager.tieneCiclos();
	}
	
	public static void cuantosConectados(){
		manager.cuantosConectados();
	}
	
	public static void agregarAGrafo(){
		manager.agregarAGrafo();
	}

}
