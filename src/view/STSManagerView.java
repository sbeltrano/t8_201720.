package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.RedBlackBST;
import model.data_structures.TablaHash;
import model.exceptions.DateNotFoundException;
import model.vo.AgencyVO;
import model.vo.StopVO;
import model.vo.VORetardo;
import model.vo.VORoute;
import model.vo.CalendarVO;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOStopTimes;
import model.vo.VOTransfer;
import model.vo.TripVO;


public class STSManagerView
{

	/**
	 * Main
	 * @param args
	 */
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar data est�tica
			case 1:

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadFiles();

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

			//1C cargar data tiempo real de bus para una fecha
			case 2:
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");

				//Fecha deseada
				String fechaCase2 = sc.next();

				//Cargar bus
				try
				{
					Controller.ITScargarTR(fechaCase2);
					System.out.println("Se cargo exit�samente la infomaci�n del bus para la feha " + fechaCase2);
				}
				catch(DateNotFoundException e)
				{
					System.out.println(e.getMessage());
				}
				
				//Controller.buscarLat();

				break;
			//1A
			case 3:
				//id de la ruta
                System.out.println("Crea un grafo");

                Controller.agregarAGrafo();
          
				
				break;

			//2A
			case 4:
				System.out.println("Indica si el grafo tiene componentes conectados");

                Controller.cuantosConectados();
                break;
			case 5:
				System.out.println("Indica si el grafo tiene ciclos");

				Controller.tieneCiclos();
				break;
            //SALIR
            case 13:

                fin = true;
                sc.close();
                break;

        }
    }
}

    /**
     * Menu
     */

private static void printMenu() {
    System.out.println("---------ISIS 1206 - Estructuras de datos----------");
    System.out.println("---------------------Taller 8----------------------");
    System.out.println("Cargar data (1C):");
    System.out.println("1. Cargar la informaci�n est�tica necesaria para la operaci�n del sistema");
    System.out.println("2. Cargar la informaci�n en tiempo real de los buses dada una fecha\n");

    System.out.println("3. Punto 1");
    System.out.println("4. Punto 2");
    System.out.println("5. Punto 3");


    System.out.println("13. Salir.\n");
    System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

}
}