package model.exceptions;

public class DateNotFoundException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public DateNotFoundException()
	{
		super("La fecha no es v�lida");
	}
}
