package model.vo;

import java.util.List;

import model.data_structures.DoubleLinkedList;

public class TripVO {
	public int routeID;
	public int serviceID;
	public int tripID;
	public String tripHead;
	public String tripShortName;
	public String directionID;
	public String blockID;
	public int shapeID;
	public String wheelchairAccessible;
	public String bikesAllowed;
	
	public double retardoTiempo=0;
	
	private DoubleLinkedList<StopsEstimServiceVO> stopsEstimService = null;	
	private DoubleLinkedList<StopVO> stop = null;	
	private DoubleLinkedList<VOStopTimes> stopTimes = null;
	private int incidentes=0;	

	
	public TripVO(int pRouteID, int pServiceID, int pTripID, String head, String shortName, String pdirectionID, String pBlockID, int pShapeID, String pWheelchairAccessible, String pBikesAllowed){
		tripID = pTripID;
		routeID = pRouteID;
		serviceID = pServiceID;
		tripHead = head;
		tripShortName = shortName;
		directionID = pdirectionID;
		blockID = pBlockID;
		shapeID = pShapeID;
		wheelchairAccessible = pWheelchairAccessible;
		bikesAllowed = pBikesAllowed;
		stopsEstimService = new DoubleLinkedList<StopsEstimServiceVO>();
		stop = new DoubleLinkedList<StopVO>();
		stopTimes = new DoubleLinkedList<VOStopTimes>();
	}
	
	public double darRetardoTiempo(){
		return retardoTiempo;
	}
	
	public void subirRetardoTiempo(double retard){
		retardoTiempo = retardoTiempo+retard;
	}
	public void addStopsEstimService(StopsEstimServiceVO p, int key){
		stopsEstimService.addLast(p,key);
	}
	
	public void addStops(StopVO p, int key){
		stop.addLast(p,key);
	}
	
	public void addStopTimes(VOStopTimes p, int key){
		stopTimes.addLast(p,key);
	}
	
	public DoubleLinkedList<StopsEstimServiceVO> darEstimService(){
		return stopsEstimService;
	}
	
	public DoubleLinkedList<StopVO> darStops(){
		return stop;
	}
	
	public DoubleLinkedList<VOStopTimes> darStopTimes(){
		return stopTimes;
	}

	public int darTripId() {
		// TODO Auto-generated method stub
		return tripID;
	}

	public int darRouteId() {
		// TODO Auto-generated method stub
		return routeID;
	}
	
	public int darServiceId(){
		return serviceID;
	}
	
	public int darShapeID(){
		return shapeID;
	}

	public void agregarIncidente() {
		// TODO Auto-generated method stub
		++incidentes;
	}
	
	public int getIncidentes(){
		return incidentes;
	}
	
}
