package model.vo;

public class BusUPVO {

	public int sequence;
	
	public BusUpdateVO stopEstimService;

	public BusUPVO( int sequence, BusUpdateVO stopEstimService) {
		super();
		
		this.sequence = sequence;
		this.stopEstimService = stopEstimService;
	}
	
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public BusUpdateVO getStopEstimService() {
		return stopEstimService;
	}

	public void setStopEstimService(BusUpdateVO stopEstimService) {
		this.stopEstimService = stopEstimService;
	}

	
}
