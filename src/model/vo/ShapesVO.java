package model.vo;

public class ShapesVO {
	private int shapeID;
	private double shapeLat;
	private double shapeLon;
	private int shapeSequense;
	private double shapeDistTravel;
	
	public ShapesVO(int shapeID, double shapeLat, double shapeLon,int shapeSequense, double shapeDistTravel) {
		super();
		this.shapeID = shapeID;
		this.shapeLat = shapeLat;
		this.shapeLon = shapeLon;
		this.shapeSequense = shapeSequense;
		this.shapeDistTravel = shapeDistTravel;
	}

	public int getShapeID() {
		return shapeID;
	}

	public double getShapeLat() {
		return shapeLat;
	}

	public double getShapeLon() {
		return shapeLon;
	}

	public int getShapeSequense() {
		return shapeSequense;
	}

	public double getShapeDistTravel() {
		return shapeDistTravel;
	}
}
