package model.vo;

public class StopEstimVO {

	public int stopCode;

	public int sequence;
	
	public StopsEstimServiceVO stopEstimService;

	public StopEstimVO(int stopCode, int sequence, StopsEstimServiceVO stopEstimService) {
		super();
		this.stopCode = stopCode;
		this.sequence = sequence;
		this.stopEstimService = stopEstimService;
	}
	
	public int getStopCode() {
		return stopCode;
	}

	public void setStopCode(int stopCode) {
		this.stopCode = stopCode;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public StopsEstimServiceVO getStopEstimService() {
		return stopEstimService;
	}

	public void setStopEstimService(StopsEstimServiceVO stopEstimService) {
		this.stopEstimService = stopEstimService;
	}


}
