package model.vo;

public class Respuesta3A {

	private int transferRouteId;
	private int tripId;

	

	private int fromStopId;
	private int tripTotalTime;
	
	public Respuesta3A(int transferRouteId, int fromStopId, int tripTotalTime, int tripIdp) {
		super();
		this.transferRouteId = transferRouteId;
		this.fromStopId = fromStopId;
		this.tripTotalTime = tripTotalTime;
		tripId = tripIdp;
	}
	
	public int getTransferRouteId() {
		return transferRouteId;
	}

	public void setTransferRouteId(int transferRouteId) {
		this.transferRouteId = transferRouteId;
	}

	public int getFromStopId() {
		return fromStopId;
	}

	public void setFromStopId(int fromStopId) {
		this.fromStopId = fromStopId;
	}

	public int getTripTotalTime() {
		return tripTotalTime;
	}

	public void setTripTotalTime(int tripTotalTime) {
		this.tripTotalTime = tripTotalTime;
	}

	public void incrementaTiempo(int seg){
		tripTotalTime += seg;
	}
	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	
}
