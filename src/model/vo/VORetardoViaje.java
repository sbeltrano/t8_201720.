package model.vo;

public class VORetardoViaje {
	
	private int viajeid;
	private double tiempoRetardoDoble;
	
	private int tiempoRetardo;
	
	public VORetardoViaje(int viajeid, int tiempoRetardo) {
		this.viajeid = viajeid;
		this.tiempoRetardo = tiempoRetardo;
	}
	public int getViajeid() {
		return viajeid;
	}
	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}
	public double getTiempoRetardoDoble() {
		return tiempoRetardoDoble;
	}
	public void setTiempoRetardoDoble(double tiempoRetardo) {
		this.tiempoRetardoDoble = tiempoRetardo;
	}
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}
	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}
	
	

}