package model.vo;


import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;

/**
 * Estructura para modelar lista req 5A
 */
public class VOPlan 
{
	//Atributos

	/**
	 * Modela las paradas en el plan
	 */
	private IDoubleLinkedList<ParadaPlanVO> secuenciaDeParadas;	


	public VOPlan() {
		super();
		secuenciaDeParadas = new DoubleLinkedList<VOPlan.ParadaPlanVO>();
	}

	//M�todos
	/**
	 * @return the secuenciaDeParadas
	 */
	public IDoubleLinkedList<ParadaPlanVO> getSecuenciaDeParadas() 
	{
		return secuenciaDeParadas;
	}

	/**
	 * @param secuenciaDeParadas the secuenciaDeParadas to set
	 */
	public void setSecuenciaDeParadas(DoubleLinkedList<ParadaPlanVO> secuenciaDeParadas)
	{
		this.secuenciaDeParadas = secuenciaDeParadas;
	}

	//Nested classes
	/**
	 * Nested class, modela una parada, con sus rutas y viajes asociados
	 */
	public class ParadaPlanVO
	{
		//Atributos

		/**
		 * Modela el id de la parada
		 */
		private String idParada;

		/**
		 * Modela las rutas que usan la parada
		 */
		private IDoubleLinkedList<RutaPlanVO> rutasAsociadasAParada;

		public ParadaPlanVO() {
			super();
			this.idParada = idParada;
			this.rutasAsociadasAParada = new DoubleLinkedList<VOPlan.ParadaPlanVO.RutaPlanVO>();
		}

		/**
		 * Estructura para modelar una ruta que usa la parada
		 */

		//M�todos
		/**
		 * @return the idParada
		 */
		public String getIdParada() 
		{
			return idParada;
		}

		/**
		 * @param idParada the idParada to set
		 */
		public void setIdParada(String idParada) 
		{
			this.idParada = idParada;
		}

		/**
		 * @return the rutasAsociadasAParada
		 */
		public IDoubleLinkedList<RutaPlanVO> getRutasAsociadasAParada()
		{
			return rutasAsociadasAParada;
		}

		/**
		 * @param rutasAsociadasAParada the rutasAsociadasAParada to set
		 */
		public void setRutasAsociadasAParada(IDoubleLinkedList<RutaPlanVO> rutasAsociadasAParada)
		{
			this.rutasAsociadasAParada = rutasAsociadasAParada;
		}

		//Nested class
		public class RutaPlanVO
		{
			/**
			 * Modela el id de la ruta
			 */
			private String idRuta;

			/**
			 * Modela los viajes en la ruta asociados a la parada
			 */
			private IDoubleLinkedList<ViajePlanVO> viajesEnRuta;

			public RutaPlanVO() {
				super();
				this.viajesEnRuta = new DoubleLinkedList<VOPlan.ParadaPlanVO.RutaPlanVO.ViajePlanVO>();
			}

			//M�todos

			/**
			 * @return the idRuta
			 */
			public String getIdRuta() 
			{
				return idRuta;
			}

			/**
			 * @param idRuta the idRuta to set
			 */
			public void setIdRuta(String idRuta)
			{
				this.idRuta = idRuta;
			}

			/**
			 * @return the viajesEnRuta
			 */
			public IDoubleLinkedList<ViajePlanVO> getViajesEnRuta() 
			{
				return viajesEnRuta;
			}

			/**
			 * @param viajesEnRuta the viajesEnRuta to set
			 */
			public void setViajesEnRuta(IDoubleLinkedList<ViajePlanVO> viajesEnRuta)
			{
				this.viajesEnRuta = viajesEnRuta;
			}

			//Nested class
			/**
			 * Estructura para modelar viaje en plan
			 */
			public class ViajePlanVO
			{
				//Atributos

				/**
				 * Modela la hora de parada en la parada asociada
				 */
				private String horaDeParada;

				/**
				 * Modela el id del viaje
				 */
				private String idViaje;

				//M�todos

				/**
				 * @return the horaDeParada
				 */
				public String getHoraDeParada() 
				{
					return horaDeParada;
				}

				/**
				 * @param horaDeParada the horaDeParada to set
				 */
				public void setHoraDeParada(String horaDeParada)
				{
					this.horaDeParada = horaDeParada;
				}

				/**
				 * @return the idViaje
				 */
				public String getIdViaje()
				{
					return idViaje;
				}

				/**
				 * @param idViaje the idViaje to set
				 */
				public void setIdViaje(String idViaje) 
				{
					this.idViaje = idViaje;
				}
			}
		}
	}
}