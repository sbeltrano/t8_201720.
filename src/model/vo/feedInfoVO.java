package model.vo;

public class feedInfoVO {
	public String feedPublisherR;
	public String feedPublisherURL;
	public String feedLang;
	public String feedStartDate;
	public String feedEndDate;
	public String feedVersion;
	public feedInfoVO(String feedPublisherR, String feedPublisherURL, String feedLang, String feedStartDate,
			String feedEndDate, String feedVersion) {
		super();
		this.feedPublisherR = feedPublisherR;
		this.feedPublisherURL = feedPublisherURL;
		this.feedLang = feedLang;
		this.feedStartDate = feedStartDate;
		this.feedEndDate = feedEndDate;
		this.feedVersion = feedVersion;
	}
	
	
}
