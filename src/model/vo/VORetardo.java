package model.vo;

/**
 * Clase que modela un retardo aplicado a una parada del viaje
 */
public class VORetardo {
	
	private int viajeid;
	private int stopid;
	

	/**
	 * Tiempo del retardo en segundos
	 */
	private double tiempoRetardoDoble;
	public VORetardo(int viajeid, int stopid, int tiempoRetardo) {
		this.viajeid = viajeid;
		this.stopid = stopid;
		this.tiempoRetardo = tiempoRetardo;
	}

	/**
	 * Tiempo del retardo en segundos
	 */
	private int tiempoRetardo;

	public int getViajeid() {
		return viajeid;
	}

	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}

	public int getStopid() {
		return stopid;
	}

	public void setStopid(int stopid) {
		this.stopid = stopid;
	}

	public double getTiempoRetardoDoble() {
		return tiempoRetardoDoble;
	}

	public void setTiempoRetardoDoble(double tiempoRetardo) {
		this.tiempoRetardoDoble = tiempoRetardo;
	}
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}

	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}
	
	

}