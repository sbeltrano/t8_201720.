package model.vo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StopsEstimServiceVO {

	private int stopCode;
	private int sequence;
	
	@SerializedName("RouteNo")
	@Expose
	private String routeNo;
	@SerializedName("RouteName")
	@Expose
	private String routeName;
	@SerializedName("Direction")
	@Expose
	private String direction;
	@SerializedName("RouteMap")
	@Expose
	private RouteMap routeMap;
	@SerializedName("Schedules")
	@Expose
	private List<Schedule> schedules = null;

	public StopsEstimServiceVO(int stopCodep, int sequencep){
		stopCode = stopCodep;
		sequence = sequencep;
	}
	public String getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public RouteMap getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(RouteMap routeMap) {
		this.routeMap = routeMap;
	}

	public List<Schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Integer getStopCode() {
		return stopCode;
	}

	public void setStopCode(Integer stopCode) {
		this.stopCode = stopCode;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

}