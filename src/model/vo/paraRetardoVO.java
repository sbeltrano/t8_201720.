package model.vo;

import java.util.List;

public class paraRetardoVO {

	private int tripid;
	
	private List<VOStopTimes> stopTimes = null;	
	private List<StopsEstimServiceVO> stopsEstimService = null;	
	private List<StopVO> stop = null;	

	
	public paraRetardoVO(int tripid, List<VOStopTimes> stopTimes, List<StopsEstimServiceVO> stopsEstimService,
			List<StopVO> stop) {
		super();
		this.setTripid(tripid);
		this.setStopTimes(stopTimes);
		this.setStopsEstimService(stopsEstimService);
		this.setStop(stop);
	}


	public int getTripid() {
		return tripid;
	}


	public void setTripid(int tripid) {
		this.tripid = tripid;
	}


	public List<VOStopTimes> getStopTimes() {
		return stopTimes;
	}


	public void setStopTimes(List<VOStopTimes> stopTimes) {
		this.stopTimes = stopTimes;
	}


	public List<StopsEstimServiceVO> getStopsEstimService() {
		return stopsEstimService;
	}


	public void setStopsEstimService(List<StopsEstimServiceVO> stopsEstimService) {
		this.stopsEstimService = stopsEstimService;
	}


	public List<StopVO> getStop() {
		return stop;
	}


	public void setStop(List<StopVO> stop) {
		this.stop = stop;
	}
	
	
}
