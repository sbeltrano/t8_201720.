package model.vo;

public class feedInfo {
	private String feedPublisher;
	private String feedPublisherURL;
	private String feedLang;
	private String feedStartDate;
	private String feedEndDate;
	private String feedVersion;
	
	public feedInfo(String feedPublisher, String feedPublisherURL,
			String feedLang, String feedStartDate, String feedEndDate,
			String feedVersion) {
		super();
		this.feedPublisher = feedPublisher;
		this.feedPublisherURL = feedPublisherURL;
		this.feedLang = feedLang;
		this.feedStartDate = feedStartDate;
		this.feedEndDate = feedEndDate;
		this.feedVersion = feedVersion;
	}

	public String getFeedPublisher() {
		return feedPublisher;
	}

	public String getFeedPublisherURL() {
		return feedPublisherURL;
	}

	public String getFeedLang() {
		return feedLang;
	}

	public String getFeedStartDate() {
		return feedStartDate;
	}

	public String getFeedEndDate() {
		return feedEndDate;
	}

	public String getFeedVersion() {
		return feedVersion;
	}
}
