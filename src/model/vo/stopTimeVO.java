package model.vo;

public class stopTimeVO {
	private int tripID;
	private String arrivalTime;
	private String departureTime;
	private int stopID;
	private int stopSecuense;
	private String stopHeadS;
	private int pickupTipe;
	private int dropoffTipe;
	private String shapeDistTrav;
	
	public stopTimeVO(int tripID, String arrivalTime, String departureTime,
			int stopID, int stopSecuense, String stopHeadS, int pickupTipe,
			int dropoffTipe, String shapeDistTrav) {
		super();
		this.tripID = tripID;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		this.stopID = stopID;
		this.stopSecuense = stopSecuense;
		this.stopHeadS = stopHeadS;
		this.pickupTipe = pickupTipe;
		this.dropoffTipe = dropoffTipe;
		this.shapeDistTrav = shapeDistTrav;
	}

	public int getTripID() {
		return tripID;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public int getStopID() {
		return stopID;
	}

	public int getStopSecuense() {
		return stopSecuense;
	}

	public String getStopHeadS() {
		return stopHeadS;
	}

	public int getPickupTipe() {
		return pickupTipe;
	}

	public int getDropoffTipe() {
		return dropoffTipe;
	}

	public String getShapeDistTrav() {
		return shapeDistTrav;
	}
}
