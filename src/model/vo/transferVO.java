package model.vo;

public class transferVO {
	private int fromStopID;
	private int toStopID;
	private int transferTipe;
	private int minTransferTime;
	
	public transferVO(int fromStopID, int toStopID, int transferTipe,
			int minTransferTime) {
		super();
		this.fromStopID = fromStopID;
		this.toStopID = toStopID;
		this.transferTipe = transferTipe;
		this.minTransferTime = minTransferTime;
	}

	public int getFromStopID() {
		return fromStopID;
	}

	public int getToStopID() {
		return toStopID;
	}

	public int getTransferTipe() {
		return transferTipe;
	}

	public int getMinTransferTime() {
		return minTransferTime;
	}
}
