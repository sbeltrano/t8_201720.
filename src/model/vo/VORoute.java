package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a route object
 */
public class VORoute {
	
	public int routeID;
	public String agencyID;
	public String routeShortName;
	public String routeLongName;
	public String routeDesc;
	public String routeType;
	public String routeURL;
	public String routeColor;
	public String routeTextColor;
	
	private DoubleLinkedList<StopsEstimServiceVO> stopsEstimService = null;	
	private DoubleLinkedList<TripVO> trips = null;	

	
	public VORoute(int id, String agency, String shortName, String longName, String Desc, String type, String URL, String color, String textColor){
		routeID=id ;
		agencyID=agency ;
		routeShortName=shortName;
		routeLongName=longName  ;
		routeDesc=Desc;
		routeType=type;
		routeURL=URL ;
		routeColor=color  ;
		routeTextColor=textColor ;
		setStopsEstimService(new DoubleLinkedList<StopsEstimServiceVO>());
		setTrips(new DoubleLinkedList<TripVO>());
	}

	public int id() {
		return routeID;
	}

	public String getAgencyID() {
		return agencyID;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public String getRouteLongName() {
		return routeLongName;
	}
	
	public String darEmpresa(){
		return agencyID;
	}
	
	public String darShortName(){
		return routeShortName;
	}
	public DoubleLinkedList<StopsEstimServiceVO> getStopsEstimService() {
		return stopsEstimService;
	}
	public void addStopsEstimService(StopsEstimServiceVO p, int key){
		stopsEstimService.addLast(p,key);
	}
	public void setStopsEstimService(DoubleLinkedList<StopsEstimServiceVO> stopsEstimService) {
		this.stopsEstimService = stopsEstimService;
	}
	public DoubleLinkedList<TripVO> getTrips() {
		return trips;
	}
	public void setTrips(DoubleLinkedList<TripVO> trips) {
		this.trips = trips;
	}
	public void addTrip(TripVO p, int key){
		trips.addLast(p,key);
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public String getRouteType() {
		return routeType;
	}

	public String getRouteURL() {
		return routeURL;
	}

	public String getRouteColor() {
		return routeColor;
	}

	public String getRouteTextColor() {
		return routeTextColor;
	}

}
