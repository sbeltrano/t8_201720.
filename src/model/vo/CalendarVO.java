package model.vo;

public class CalendarVO {
	
	private int serviceID;
	private int monday;
	private int tuesday;
	private int wednesday;
	private int thursday;
	private int friday;
	private int saturday;
	private int sunday;
	private int startDate;
	private int endDate;
	
	public CalendarVO(int serviceID, int monday, int tuesday, int wednesday,int thursday, int friday, int saturday, int sunday,
			int startDate, int endDate) {
		super();
		this.serviceID = serviceID;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public int getServiceID() {
		return serviceID;
	}

	public int getMonday() {
		return monday;
	}

	public int getTuesday() {
		return tuesday;
	}

	public int getWednesday() {
		return wednesday;
	}

	public int getThursday() {
		return thursday;
	}

	public int getFriday() {
		return friday;
	}

	public int getSaturday() {
		return saturday;
	}

	public int getSunday() {
		return sunday;
	}

	public int getStartDate() {
		return startDate;
	}

	public int getEndDate() {
		return endDate;
	}
}
