package model.vo;

public class BusUpdateVO {

	private String VehicleNo;
	private long TripId;
	private String RouteNo;
	private String Direction;
	private String Destination;
	private String Pattern;
	private double Latitude;
	private double Longitude;
	private String RecordedTime;
	private RouteMapVO RouteMap;
	
	
	

	 public BusUpdateVO(String vehicleNo, long tripId, String routeNo,
			String direction, String destination, String pattern,
			double latitude, double longitude, String recordedTime,
			RouteMapVO routeMap) {
		super();
		VehicleNo = vehicleNo;
		TripId = tripId;
		RouteNo = routeNo;
		Direction = direction;
		Destination = destination;
		Pattern = pattern;
		Latitude = latitude;
		Longitude = longitude;
		RecordedTime = recordedTime;
		RouteMap = routeMap;
	}
	public String getVehicleNumber(){
		 return VehicleNo;
	 }
	 public long getTripId(){
		 return TripId;
	 }
	 public String getRouteNo(){
		 return RouteNo;
	 }
	public String getDirection() {
		return Direction;
	}
	public String getDestination() {
		return Destination;
	}

	public String getPattern() {
		return Pattern;
	}

	public double getLatitude() {
		return Latitude;
	}

	public double getLongitude() {
		return Longitude;
	}

	public String getRecordedTime() {
		return RecordedTime;
	}

	public RouteMapVO getRouteMap() {
		return RouteMap;
	}


}
