package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.SingleList;
import model.data_structures.LinkedlistIS;


public class StopVO {

	
	public int stopID;
	public String stopCode;
	public String stopName;
	public String stopDesc;
	public String stopLat;
	public String stopLon;
	public String zoneID;
	public String stopURL;
	public String locationType;
	public String parentStation;
	private int numeroIncidentes=0;
	private DoubleLinkedList<VOStopTimes> stopTimes = null;
	private DoubleLinkedList<TripVO> tripsEnStops = null;
	private DoubleLinkedList<StopsEstimServiceVO> stopEstim = null;
	private DoubleLinkedList<VORoute> routes = null;
	private SingleList<TripVO> trip = null;

	public StopVO(int id1, String stopCode1, String name1, String Desc, String lat, String lon, String pZoneID, String URL, String locT, String parentS){
		stopID = id1;
		stopCode = stopCode1;
		stopName = name1;
		stopDesc = Desc;
		stopLat = lat;
		stopLon = lon;
		zoneID = pZoneID;
		stopURL = URL;
		locationType = locT;
		parentStation = parentS;
		tripsEnStops=(new DoubleLinkedList<TripVO>());
		stopTimes=(new DoubleLinkedList<VOStopTimes>());
		stopEstim=new DoubleLinkedList<StopsEstimServiceVO>();
		setRoutes(new DoubleLinkedList<VORoute>());
		trip = new SingleList<TripVO>();
	}
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopID;
	}
	
	public String getStopCode(){
		return stopCode;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}
	
	public double getLat(){
		//System.out.println(Double.parseDouble(stopLat));
		return Double.parseDouble(stopLat);
	}
	
	public double getLong(){
		return Double.parseDouble(stopLon);
	}
	
	public int getNumeroIncidentes( )
	{
		return numeroIncidentes;
	}
	
	public void addNumeroIncidentes(int pNumInci)
	{
		numeroIncidentes = pNumInci+numeroIncidentes;
	}
	public DoubleLinkedList<VOStopTimes> getStopTimes() {
		return stopTimes;
	}
	public void setStopTimes(DoubleLinkedList<VOStopTimes> stopTimes) {
		this.stopTimes = stopTimes;
	}
	public void addStopTimes(VOStopTimes p, int key){
		stopTimes.addFirst(p,key);
	}
	public DoubleLinkedList<TripVO> getTripsEnStops() {
		return tripsEnStops;
	}
	public void setTripsEnStops(DoubleLinkedList<TripVO> tripsEnStops) {
		this.tripsEnStops = tripsEnStops;
	}
	public void addTrip (TripVO p, int key){
		tripsEnStops.addLast(p,key);
	}
	public DoubleLinkedList<StopsEstimServiceVO> getStopEstim() {
		return stopEstim;
	}
	public void setStopEstim(DoubleLinkedList<StopsEstimServiceVO> stopEstim) {
		this.stopEstim = stopEstim;
	}
	
	public void addEstim(StopsEstimServiceVO p, int key){
		stopEstim.addLast(p,key);
	}
	public DoubleLinkedList<VORoute> getRoutes() {
		return routes;
	}
	public void setRoutes(DoubleLinkedList<VORoute> routes) {
		this.routes = routes;
	}
	
	public void addRoutes(VORoute p, int key){
		routes.addLast(p, key);
	}
	
	public void buscarTripsEnRoutes(){
		routes.ponerActualPrimero();
		System.out.println("routes"+routes.size());

		System.out.println("trips"+tripsEnStops.size());
		while (routes.hasNext()) {
			tripsEnStops.ponerActualPrimero();
			while (tripsEnStops.hasNext()) {
				//System.out.println("entro");
				if (routes.darActual().id()==tripsEnStops.darActual().darRouteId()) {
					routes.darActual().addTrip(tripsEnStops.darActual(),tripsEnStops.darActual().routeID);
				}
				tripsEnStops.iterateOne();
			}
			routes.iterateOne();
		}
	}
	public void ordenarTripsPorRuta(){
		
	}
	public SingleList<TripVO> getTrip() {
		return trip;
	}
	public void setTrip(SingleList<TripVO> trip) {
		this.trip = trip;
	}
	
	public void addATrip(TripVO p){
		trip.add(p);
	}

}
