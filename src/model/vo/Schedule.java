package model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {

	@SerializedName("Pattern")
	@Expose
	private String pattern;
	@SerializedName("Destination")
	@Expose
	private String destination;
	@SerializedName("ExpectedLeaveTime")
	@Expose
	private String expectedLeaveTime;
	@SerializedName("ExpectedCountdown")
	@Expose
	private Integer expectedCountdown;
	@SerializedName("ScheduleStatus")
	@Expose
	private String scheduleStatus;
	@SerializedName("CancelledTrip")
	@Expose
	private Boolean cancelledTrip;
	@SerializedName("CancelledStop")
	@Expose
	private Boolean cancelledStop;
	@SerializedName("AddedTrip")
	@Expose
	private Boolean addedTrip;
	@SerializedName("AddedStop")
	@Expose
	private Boolean addedStop;
	@SerializedName("LastUpdate")
	@Expose
	private String lastUpdate;

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getExpectedLeaveTime() {
		return expectedLeaveTime;
	}

	public void setExpectedLeaveTime(String expectedLeaveTime) {
		this.expectedLeaveTime = expectedLeaveTime;
	}

	public Integer getExpectedCountdown() {
		return expectedCountdown;
	}

	public void setExpectedCountdown(Integer expectedCountdown) {
		this.expectedCountdown = expectedCountdown;
	}

	public String getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	public Boolean getCancelledTrip() {
		return cancelledTrip;
	}

	public void setCancelledTrip(Boolean cancelledTrip) {
		this.cancelledTrip = cancelledTrip;
	}

	public Boolean getCancelledStop() {
		return cancelledStop;
	}

	public void setCancelledStop(Boolean cancelledStop) {
		this.cancelledStop = cancelledStop;
	}

	public Boolean getAddedTrip() {
		return addedTrip;
	}

	public void setAddedTrip(Boolean addedTrip) {
		this.addedTrip = addedTrip;
	}

	public Boolean getAddedStop() {
		return addedStop;
	}

	public void setAddedStop(Boolean addedStop) {
		this.addedStop = addedStop;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}

