package model.vo;

import model.data_structures.IDoubleLinkedList;

public class VORangoHora {
	
	private int horaInicial;
	private int horaFinal;
	
	/**
	 * Lista de retardos ordenados por tiempo total de retardo
	 */
	IDoubleLinkedList<VORetardo> listaRetardos;
	
	public VORangoHora(int horaInicial, int horaFinal, IDoubleLinkedList<VORetardo> listaRetardos) {
		super();
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
		this.listaRetardos = listaRetardos;
	}

	public int getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(int horaInicial) {
		this.horaInicial = horaInicial;
	}

	public int getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	public IDoubleLinkedList<VORetardo> getListaRetardos() {
		return listaRetardos;
	}

	public void setListaRetardos(IDoubleLinkedList<VORetardo> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}

}
