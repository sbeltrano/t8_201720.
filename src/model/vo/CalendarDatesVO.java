package model.vo;

public class CalendarDatesVO {
	
	private int serviceID;
	private String date;
	private int exceptionTipe;
	
	public CalendarDatesVO(int serviceID, String date, int exceptionTipe) {
		super();
		this.serviceID = serviceID;
		this.date = date;
		this.exceptionTipe = exceptionTipe;
	}

	public int getServiceID() {
		return serviceID;
	}

	public String getDate() {
		return date;
	}

	public int getExceptionTipe() {
		return exceptionTipe;
	}
}
