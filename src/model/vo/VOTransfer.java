package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;

public class VOTransfer
{
	private int fromStopID;
	private int toStopID;
	private int tipe;
	private int minTipe;
	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IDoubleLinkedList<StopVO> listadeParadas;

	public VOTransfer(int fromStopID, int toStopID, int transferType,int minTransferTime) {
		this.setFromStopID(fromStopID);
		this.setToStopID(toStopID);
		this.setTipe(transferType);
		this.setMinTipe(minTransferTime);
		listadeParadas = new DoubleLinkedList<StopVO>();
	}

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public IDoubleLinkedList<StopVO> getListadeParadas()
	{
		return listadeParadas;
	}

	public void addAListaParadas (StopVO p){
		//listadeParadas.addLast(p);
	}
	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IDoubleLinkedList<StopVO> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}

	public int getFromStopID() {
		return fromStopID;
	}

	public void setFromStopID(int fromStopID) {
		this.fromStopID = fromStopID;
	}

	public int getToStopID() {
		return toStopID;
	}

	public void setToStopID(int toStopID) {
		this.toStopID = toStopID;
	}

	public int getTipe() {
		return tipe;
	}

	public void setTipe(int tipe) {
		this.tipe = tipe;
	}

	public int getMinTipe() {
		return minTipe;
	}

	public void setMinTipe(int minTipe) {
		this.minTipe = minTipe;
	}
	

}
