package model.vo;

public class AgencyVO {
	
	private String agencyID;
	private String agencyName;
	private String agencyURL;
	private String agencyTimeZone;
	private String agencyLang;
	
	private int distanciaRecorrida;
	
	public AgencyVO(String agencyID, String agencyName, String agencyURL,
			String agencyTimeZone, String agencyLang) {
		super();
		this.agencyID = agencyID;
		this.agencyName = agencyName;
		this.agencyURL = agencyURL;
		this.agencyTimeZone = agencyTimeZone;
		this.agencyLang = agencyLang;
	}

	public String getAgencyID() {
		return agencyID;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public String getAgencyURL() {
		return agencyURL;
	}

	public String getAgencyTimeZone() {
		return agencyTimeZone;
	}

	public String getAgencyLang() {
		return agencyLang;
	}
	

	public int getDistanciaRecorrida() {
		return distanciaRecorrida;
	}
}
