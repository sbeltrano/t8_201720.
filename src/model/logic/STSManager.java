package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ISTSMAnager;
import model.data_structures.CC;
import model.data_structures.Cycle;
import model.data_structures.DiGraph;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.Node;
import model.data_structures.IDoubleLinkedList;
import static java.lang.Math.toIntExact;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MaxPQ;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingHashST;
import model.data_structures.SingleList;
import model.data_structures.Stack;
import model.data_structures.TablaHash;
import model.exceptions.TripNotFoundException;
import model.vo.AgencyVO;
import model.vo.BusUPVO;
import model.vo.BusUpdateVO;
import model.vo.CalendarDatesVO;
import model.vo.CalendarVO;
import model.vo.Respuesta3A;
import model.vo.Schedule;
import model.vo.ShapesVO;
import model.vo.StopEstimVO;
import model.vo.StopVO;
import model.vo.StopsEstimServiceVO;
import model.vo.VORetardoViaje;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VOTransfer;
import model.vo.TripVO;
import model.vo.VORetardo;
import model.vo.VORoute;
import model.vo.VOStopTimes;
import model.vo.VOTransfer;
import model.vo.feedInfoVO;
import model.vo.stopTimeVO;
import model.vo.transferVO;

public class STSManager implements ISTSMAnager {
	private BusUpdateVO[] busUpdates1;
	private StopsEstimServiceVO[] busEstimService1;
	private IDoubleLinkedList<StopsEstimServiceVO> busEstimSeerviceList;
	private IDoubleLinkedList<AgencyVO> agencyList;
	//private IDoubleLinkedList<TripVO> tripList;
	private IDoubleLinkedList<CalendarVO> calendarList;
	private IDoubleLinkedList<CalendarDatesVO> calendaDatesList;
	private IDoubleLinkedList<VORoute> routesList;
	private IDoubleLinkedList<ShapesVO> shapesList;
	private IDoubleLinkedList<feedInfoVO> feedInfoList;
	private IDoubleLinkedList<VOStopTimes> stopTimesList;
	private IDoubleLinkedList<VOTransfer>transfersList;
	
	private IDoubleLinkedList<BusUpdateVO> busUpdatesList;
	private IDoubleLinkedList<StopVO> stops3;
	private IQueue<BusUpdateVO> busUpdates;
	private IStack<StopVO> stops;
	private IDoubleLinkedList<VORetardo> retardos;
	private IDoubleLinkedList<VORetardoViaje> retardosViajeList;
	
	private TablaHash<Integer, TripVO> tripsHash;
	private TablaHash<Double, BusUPVO> busUpdatesHash;
	private TablaHash<Integer, StopVO> stopsHash;
	private TablaHash<Integer, StopVO> stopsConCodeHash;
	private TablaHash<String, VOStopTimes> stopTimesHash;
	private TablaHash<String, StopEstimVO > stopEstimHash;
	private TablaHash<Integer, VORoute > routeHash;
	private TablaHash<String, ShapesVO> shapesHash;
	private RedBlackBST<String, ShapesVO> shapesBST;
	private TablaHash<String, VOTransfer> transferHash;
	private RedBlackBST<Integer, CalendarVO> calendarBST;
	private TablaHash<String, VORoute> routeHashShort;
	
	private DiGraph<Integer, VOStopTimes> stopTimesDiGraph;
	
	public STSManager() {
		//stops = new Stack<StopVO>();
		busUpdates = new Queue<BusUpdateVO>();
		busEstimSeerviceList = new DoubleLinkedList<StopsEstimServiceVO>();
		stops3 = new DoubleLinkedList<StopVO>();
		busUpdatesList = new DoubleLinkedList<BusUpdateVO>();
		agencyList = new DoubleLinkedList<AgencyVO>();
		//tripList = new DoubleLinkedList<TripVO>();
		calendarList = new DoubleLinkedList<CalendarVO>();
		calendaDatesList = new DoubleLinkedList<CalendarDatesVO>();
		routesList = new DoubleLinkedList<VORoute>();
		shapesList = new DoubleLinkedList<ShapesVO>();
		feedInfoList = new DoubleLinkedList<feedInfoVO>();
		stopTimesList = new DoubleLinkedList<VOStopTimes>();
		transfersList = new DoubleLinkedList<VOTransfer>();
		retardos = new DoubleLinkedList<VORetardo>();
		
		tripsHash = new TablaHash<Integer, TripVO>();
		busUpdatesHash = new TablaHash<Double, BusUPVO>();
		stopsHash = new TablaHash<Integer, StopVO>();
		stopTimesHash = new TablaHash<String, VOStopTimes>();
		stopEstimHash = new TablaHash<String, StopEstimVO>();
		routeHash =new TablaHash<Integer, VORoute >();
		routeHashShort = new TablaHash<String, VORoute>();
		shapesHash= new TablaHash<String, ShapesVO>();
		transferHash = new TablaHash<String, VOTransfer >();
		stopsConCodeHash = new TablaHash<Integer, StopVO>();
		shapesBST = new RedBlackBST<String, ShapesVO>();
		calendarBST = new RedBlackBST<Integer, CalendarVO>();
		
		stopTimesDiGraph = new DiGraph<Integer, VOStopTimes>();
	}
	
	public int horaInt(String hora){
		String[] partes = hora.split(":");
		String horas = "";
		if (partes[0].startsWith(" ")) {
			partes[0].replaceAll("\\s","");
			horas = "0"+(partes[0]);
		}
		else horas = (partes[0]);
		horas.replaceAll("\\s","");
		String minutos = (partes[1]);
		String segundos = (partes[2]);
		return Integer.parseInt((horas+minutos+segundos).replaceAll("\\s",""));
	}
	
	
	
	public int diferenciaTiempo(String busServiceTime, String stopExpectedTime){
		int sExpectedTime = horaInt(stopExpectedTime);
		int value = Integer.parseInt(busServiceTime.replaceAll("[^0-9]", ""));
		return value - sExpectedTime;
	}
	

	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub
		 BufferedReader reader = null;
		 try {
			 reader = new BufferedReader(new FileReader(rtFile));
		     Gson gson = new GsonBuilder().create();

		     String partes[] = rtFile.getName().split("_");
		     System.out.println(partes.length);
		     String ultimo = partes[partes.length-1];
		     System.out.println(ultimo);
		     String stopCySequence[] = ultimo.split(".json");
		     System.out.println("tamaño de stpSeq"+stopCySequence.length);
		     int sequence = Integer.parseInt(stopCySequence[0]);
		     
		     busUpdates1 = gson.fromJson(reader, BusUpdateVO[].class);
		     //System.out.println(busUpdates1.length);
		     for (int i = 0; i < busUpdates1.length; i++) {
				//busUpdates.enqueue(busUpdates1[i]);
				//busUpdatesList.addLast(busUpdates1[i],horaInt(busUpdates1[i].getRecordedTime()));
		    	 BusUPVO busUpVo = new BusUPVO(sequence, busUpdates1[i]);
		    	 
				busUpdatesHash.put(busUpdates1[i].getLatitude(), busUpVo);
			}
		    
		} catch (Exception e) {
			// TODO: handle exception
		     System.out.println("payla");

		}
		 //System.out.println(busUpdates.darUltimo().getTripId()); 
		 System.out.println("busUpdates size: "+busUpdatesHash.size());
	}
	
	public void readStopsEstimService(File rtFile){
		BufferedReader reader = null;
		 try {
			 reader = new BufferedReader(new FileReader(rtFile));
			 //String linea = reader.readLine();
		     Gson gson = new GsonBuilder().create();
		     
		     String partes[] = rtFile.getName().split("_");
		     //System.out.println(partes.length);
		     String ultimo = partes[partes.length-1];
		     //System.out.println(ultimo);
		     String stopCySequence[] = ultimo.split(".json");
		     //System.out.println("tamaño de stpSeq"+stopCySequence.length);
		     String ultimoPartes[] = stopCySequence[0].split("-");
		     int stopCode = Integer.parseInt(ultimoPartes[0]);
		     int sequence = Integer.parseInt(ultimoPartes[0]);
		     
		     busEstimService1 = gson.fromJson(reader, StopsEstimServiceVO[].class);
		     //System.out.println(busEstimService1.length);
		     //System.out.println(busEstimService1[0].getRouteName());
		     for (int i = 0; i < busEstimService1.length; i++) {
		    	 String routeNo = busEstimService1[i].getRouteNo();
			     StopEstimVO st = new StopEstimVO(stopCode, sequence,busEstimService1[i] );
			     String keys = routeNo + "-" + Integer.toString(stopCode) + "-"+Integer.toString(sequence);
			     stopEstimHash.put(keys, st);
			}
		     //System.out.println(busEstimSeerviceList.size());
		     
		    
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		    System.out.println("hubo un error o no hay dato");

		}
	}
	
	
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
        
        return distance;
 
    }

  private Double toRad(Double value) {
        return value * Math.PI / 180;
    }


	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopsFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String ID = partes[0];
					int stopID = Integer.parseInt(ID);
					String stopCode = partes[1];
					String stopName = partes[2];
					String stopDesc = partes[3];
					String stopLat = partes[4];
					String stopLon = partes[5];
					String zoneID = partes[6];
					String stopURL = partes[7];
					String locationType = partes[8];
					String parentStation = "";
					if (partes.length > 9) {
						parentStation = partes[9];
					}
					
					StopVO stop = new StopVO(stopID,stopCode,stopName,stopDesc,stopLat,stopLon,zoneID,stopURL,locationType,parentStation);
					stops3.addLast(stop,stopID);
					stopsHash.put(stopID, stop);
					if (stopCode.equals(" ")) {
						stopsConCodeHash.put(0, stop);
					}
					else {
						stopsConCodeHash.put(Integer.parseInt(stopCode), stop);
					}
					
					//System.out.println(stops.darPrimero().getName());
					linea = lector.readLine( );
				}
				lector.close( );
			}
			catch( IOException e )
			{
				System.out.print("que pazo stops");
			}
		}
	}
	public void loadAgencies(String agencyFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( agencyFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String agencyID = partes[0];
					String agencyName = partes[1];
					String agencyURL = partes[2];
					String agencyTimeZone = partes[3];
					String agencyLang = partes[4];
					
					AgencyVO agency = new AgencyVO(agencyID, agencyName, agencyURL, agencyTimeZone, agencyLang);
					agencyList.addLast(agency, 0);
					//System.out.println(agency.getAgencyID());
					linea = lector.readLine( );
				}
				lector.close( );
			}
			catch( IOException e )
			{
				System.out.print("que pazo agencies");
			}
		}
	}

	//@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( tripsFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String sID = partes[0];
					int routeID = Integer.parseInt(sID);
					int serviceID = Integer.parseInt(partes[1]);
					String tID = partes[2];
					int tripID = Integer.parseInt(tID);
					String tripHeadSign = partes[3];
					String tripShortName = partes[4];
					String directionID = partes[5];
					String blockID = partes[6];
					int shapeID = Integer.parseInt(partes[7]);
					String wheelChair = partes[8];
					String bikesAllowed = "";
					if (partes.length > 9) {
						bikesAllowed = partes[9];
					}
					TripVO trip = new TripVO(routeID, serviceID, tripID, tripHeadSign, tripShortName, directionID, blockID, shapeID, wheelChair, bikesAllowed);
					//tripList.addLast(trip,tripID);
					tripsHash.put(tripID, trip);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso trips");
			}
		}
		//agregarParadasATrips();
	}
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String tID = partes[0];
					int tripID = Integer.parseInt(tID);
					String arrivalTime = partes[1];
					String departureTime = partes[2];
					String sID = partes[3];
					int stopID = Integer.parseInt(sID);
					String stopSequence = partes[4];
					String stopHeadsign = partes[5];
					String pickupType = partes[6];
					String dropOffType = partes[7];
					String shapeDistTraveled = "";
					if (partes.length > 8) {
						shapeDistTraveled = partes[8];
					}
					
					VOStopTimes stopTime = new VOStopTimes(tripID,arrivalTime,departureTime,stopID,stopSequence,stopHeadsign,pickupType,dropOffType,shapeDistTraveled);
					stopTimesList.addLast(stopTime,tripID);
					String p = tripID + "-"+stopID+"-"+arrivalTime;
					stopTimesHash.put(p, stopTime);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso stoptimes");

			}
		}
		
	}
	
	public void loadCalendar(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					int serviceID = Integer.parseInt(partes[0]);
					int monday = Integer.parseInt(partes[1]);
					int tuesday = Integer.parseInt(partes[2]);
					int wednesday = Integer.parseInt(partes[3]);
					int thursday = Integer.parseInt(partes[4]);
					int friday = Integer.parseInt(partes[5]);
					int saturday = Integer.parseInt(partes[6]);
					int sunday = Integer.parseInt(partes[7]);
					int startDate = Integer.parseInt(partes[8]);
					int endDate = Integer.parseInt(partes[9]);

					
					CalendarVO calendar = new CalendarVO(serviceID, monday, tuesday, wednesday, thursday, friday, saturday, sunday, startDate, endDate)	;				
					calendarList.addLast(calendar,serviceID);
					calendarBST.put(serviceID, calendar);
					
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso calendar");

			}
		}
		
	}
	
	public void loadCalendarDates(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					int serviceID = Integer.parseInt(partes[0]);
					String date = (partes[1]);
					int exceptionTipe = Integer.parseInt(partes[2]);
					
					CalendarDatesVO	calendarDates = new CalendarDatesVO(serviceID, date, exceptionTipe);			
					calendaDatesList.addLast(calendarDates,serviceID);
					
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso calendrdates");

			}
		}
		
	}
	
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( routesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String ID = partes[0];
					int routeID = Integer.parseInt(ID);
					String agencyID = partes[1];
					String routeShortName = partes[2];
					String routeLongName = partes[3];
					String routeDesc = partes[4];
					String routeType = partes[5];
					String routeURL = partes[6];
					String routeColor = partes[7];
					String routeTextColor = "";
					if (partes.length>8) {
						routeTextColor=partes[8];
					}
					VORoute rout=new VORoute(routeID, agencyID, routeShortName, routeLongName, routeDesc, routeType, routeURL, routeColor, routeTextColor);
					routesList.addLast(rout,routeID);
					routeHash.put(routeID, rout);
					routeHashShort.put(routeShortName, rout);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso route");
			}
		}
		//agregarTripsARoads();
		
	}
	
	
	public void loadShapes(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					int shapeID = Integer.parseInt(partes[0]);
					double shapeLat = Double.parseDouble(partes[1]);
					double shapeLon = Double.parseDouble(partes[2]);
					int shapeSequence = Integer.parseInt(partes[3]);
					double shapeDistTraveled = Double.parseDouble(partes[4]);
					
					ShapesVO shape = new ShapesVO(shapeID, shapeLat, shapeLon, shapeSequence, shapeDistTraveled);
					String shapeid1=String.format ("%07d",shapeID);
					int secuenciaTransf = 9999 - shapeSequence;
					String shapesequence = String.format ("%04d", secuenciaTransf);
					shapesList.addLast(shape,shapeID);
					String p = shapeid1 + shapesequence;
					shapesHash.put(p, shape);
					shapesBST.put(p, shape);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
				System.out.println("que paso shapes");

			}
		}
		
	}

	
	public void loadFeedInfo(String agencyFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( agencyFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String feedPublisherR = partes[0];
					String feedPublisherURL = partes[1];
					String feedLang = partes[2];
					String feedStartDate = partes[3];
					String feedEndDate = partes[4];
					String feedVersion = partes[5];
					
					feedInfoVO feed = new feedInfoVO(feedPublisherR, feedPublisherURL, feedLang, feedStartDate, feedEndDate, feedVersion);
					feedInfoList.addLast(feed,0);
					linea = lector.readLine( );
				}
				lector.close( );
			}
			catch( IOException e )
			{
				System.out.print("que pazo feed");
			}
		}
	}
	
	
	public void loadTransfers(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					int fromStopID = Integer.parseInt(partes[0]);
					int toStopID = Integer.parseInt(partes[1]);
					int transferType = Integer.parseInt(partes[2]);
					int minTransferType = 0;
					if (partes.length>3) {
						minTransferType = Integer.parseInt(partes [3]);
					}

					VOTransfer transfer = new VOTransfer(fromStopID, toStopID, transferType, minTransferType);
					transfersList.addLast(transfer,0);
					String key = fromStopID + "-" + toStopID;
					transferHash.put(key , transfer);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );

			}
			catch( IOException e )
			{
				System.out.println("que paso transfers");

			}
		}
		
	}

	
	/**
	 * ===============================================================================
	 * Taller 8
	 * ===============================================================================
	 */
	public void agregarAGrafo(){
		TablaHash<Integer, VOStopTimes> stopTimes = new TablaHash<>();
		int veinte = 0;
		int conteo = 1;
		for (String s : stopTimesHash.keys()) {
			if ( stopTimesHash.get(s).darTripId()==9017993) {
				stopTimesDiGraph.addVertex(stopTimesHash.get(s).darStopId(), stopTimesHash.get(s));
				stopTimes.put(Integer.parseInt(stopTimesHash.get(s).getStopSequence()), stopTimesHash.get(s));
				System.out.println("stopsequence "+stopTimesHash.get(s).getStopSequence());

				++veinte;
			}
		}
		System.out.println("size "+stopTimesDiGraph.darVertices().size());
		for (Integer s : stopTimes.keys()) {
			double d = (Double.parseDouble(stopTimes.get(conteo+1).getShapeDistTraveled()));
			if (conteo<19) {
				stopTimesDiGraph.addEdge(stopTimes.get(conteo).darStopId(), stopTimes.get(conteo+1).darStopId(), Double.parseDouble(stopTimes.get(conteo+1).getShapeDistTraveled()));
				++conteo;
			}
			
		}
		
	}
	
	public void cuantosConectados(){
		CC cc1 = new CC(stopTimesDiGraph);
		System.out.println("Numero de componentes conectados: "+cc1.count());
	}
	
	public void tieneCiclos(){
		Cycle ciclo = new Cycle(stopTimesDiGraph);
		if (ciclo.hayCiclo()) {
			System.out.println("Si hay mas de un ciclo: "); 
			for (int i = 0; i < ciclo.darCiclo().size(); i++) {
				System.out.println("ciclo: "+ciclo.darCiclo().pop().toString()); 
			}
		}
		else System.out.println("El grafo no tiene ciclos");
	}

	@Override
	public void buscarLat() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cargarRetardosViaje() {
		// TODO Auto-generated method stub
		
	}
	
}
