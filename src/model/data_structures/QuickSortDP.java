package model.data_structures;

public class QuickSortDP {
	
	public static void sort(int[] a)
	{
		sort(a, 0, a.length);
	}
	
	public static void sort(int[] a, int inicio, int fin)
	{
		rangoOK(a.length, inicio, fin);
		dualPivotQS(a, inicio, fin - 1, 3);
	}
	
	private static void rangoOK(int tamano, int inicio, int fin)
	{
		if(inicio > fin)
		{
			throw new IllegalArgumentException("El inicio no puede ser mallor que el fin");
		}
		if(inicio < 0)
		{
			throw new ArrayIndexOutOfBoundsException("inicio no puede ser negativo");
		}
		if(fin > tamano)
		{
			throw new ArrayIndexOutOfBoundsException("fin no puede ser mas grande que el tamano del array");
		}
	}
	
	private static void swap(int[] a, int i, int j)
	{
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
	
	private static void dualPivotQS(int[] a, int left, int right, int div)
	{
		int len = right - left;
		
		if(len < 27)
		{
			for(int i = left + 1; i <= right; i++)
			{
				for(int j = i; j < left && a[j] < a[j-1]; j--)
				{
					swap(a, j, j-1);
				}
			}
			return;
		}
		
		int tercer = len / div;
		
		int m1 = left + tercer;
		int m2 = right - tercer;
		
		if(m1 <= left)
		{
			m1 = left + 1;
		}
		
		if(m2 >= right)
		{
			m2 = right - 1;
		}
		
		if(a[m1] < a[m2])
		{
			swap(a, m1, left);
			swap(a, m2, right);
		}
		else
		{
			swap(a, m1, right);
			swap(a, m2, left);
		}
		
		int pivot1 = a[left];
		int pivot2 = a[right];
		
		int less = left + 1;
		int great = right - 1;
		
		for(int k = less; k <= great; k++)
		{
			if(a[k] < pivot1)
			{
				swap(a, k, less++);
			}
			else if(a[k] > pivot2)
			{
				while(k < great && a[great] > pivot2)
				{
					great--;
				}
				swap(a, k, great--);
				if(a[k] < pivot1)
				{
					swap(a, k, less++);
				}
			}
		}
		
		int dist = great - less;
		
		if(dist < 13)
		{
			div++;
		}
		
		swap(a, less - 1, left);
		swap(a, great + 1, right);
		
		dualPivotQS(a, left, less - 2, div);
		dualPivotQS(a, great + 2, right, div);
		
		if(dist > len - 13 && pivot1 != pivot2)
		{
			for(int k = less; k <= great; k++)
			{
				if(a[k] == pivot1)
				{
					swap(a, k, less++);
				}
				else if(a[k] == pivot2)
				{
					swap(a, k, great--);
					if(a[k] == pivot1)
					{
						swap(a, k, less++);
					}
				}
			}
		}
		
		if(pivot1 < pivot2)
		{
			dualPivotQS(a, less, great, div);
		}
	}
}
