package model.data_structures;

import model.exceptions.TripNotFoundException;
import java.util.Iterator;
import java.util.function.Consumer;

public class DoubleLinkedList<E> implements IDoubleLinkedList<E>,Iterator<E> {
	private Node head;
    private Node tail;
    private int size;
    private Node actual;
     
    public DoubleLinkedList() {
        size = 0;
    }
    /**
     * this class keeps track of each element information
     * @author java2novice
     *
     */
    public static class Node<E> {
    	public int data;
        public E element;
        public Node next;
        public Node prev;
 
        public Node(E element, Node next, Node prev, int d) {
        	data = d;
            this.element = element;
            this.next = next;
            this.prev = prev;
            
        }
    }
    /**
     * returns the size of the linked list
     * @return
     */
    public int size() { return size; }
    
    public int realSize( )
    {
    	int re = 0;
    	this.ponerActualPrimero();
    	while(this.hasNext())
    	{
    		re++;
    		this.iterateOne();
    	}
    	return re;
    }
     
    /**
     * return whether the list is empty or not
     * @return
     */
    public boolean isEmpty() { return size == 0; }
     
    /**
     * adds element at the starting of the linked list
     * @param element
     */
    public void addFirst(E element, int d) {
        Node tmp = new Node(element, head, null,d);
        if(head != null ) {head.prev = tmp;}
        actual = tmp;
        head = tmp;
        if(tail == null) { tail = tmp;}
        size++;
        //System.out.println("adding: "+element);
    }
     
    /**
     * adds element at the end of the linked list
     * @param element
     */
    public void addLast(E element, int d) {
         
        Node tmp = new Node(element, null, tail,d);
        if(tail != null) {tail.next = tmp;}
        tail = tmp;
        if(head == null) { head = tmp;
        actual = tmp;
        }
        size++;
        //System.out.println("adding: "+element);
    }
     
    /**
     * this method walks forward through the linked list
     */
    public void iterateForward(){
         
        System.out.println("iterating forward..");
        Node tmp = head;
        while(tmp != null){
            //System.out.println(tmp.element);
            tmp = tmp.next;
        }
        //actual = tmp;
    }
     
    /**
     * this method walks backward through the linked list
     */
    public void iterateBackward(){
         
        //System.out.println("iterating backword..");
        Node tmp = tail;
        while(tmp != null){
            System.out.println(tmp.element);
            tmp = tmp.prev;
        }
    }
     
    /**
     * this method removes element from the start of the linked list
     * @return
     */
    public E removeFirst() {
        if (size == 0)
			try {
				throw new TripNotFoundException();
			} catch (TripNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        Node tmp = head;
        head = head.next;
        actual = head.next;
        head.prev = null;
        size--;
        //System.out.println("deleted: "+tmp.element);
        return (E) tmp.element;
    }
     
    /**
     * this method removes element from the end of the linked list
     * @return
     */
    public E removeLast() {
        if (size == 0)
			try {
				throw new TripNotFoundException();
			} catch (TripNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        Node tmp = tail;
        tail = tail.prev;
        actual = tail.prev;
        tail.next = null;
        size--;
        //System.out.println("deleted: "+tmp.element);
        return (E) tmp.element;
    }
    
    
    public boolean hasNext( )
    {

    	if (actual == null) {
			return false;
		}
    	else {
			return true;
		}

    }
    
    public boolean hasNext2( )
    {

    	if (actual.next == null) {
			return false;
		}
    	else {
			return true;
		}

    }
    
    public void iterateOne ( )
    {
    	
    	actual = actual.next;
    	
    }
    
    public void iteratePreviousOne(){
    	actual=actual.prev;
    }
    
    public void ponerActualPrimero(){
    	actual = null;
    	actual = head;
    }
    public E darActual( )
    {
    	E e = (E) actual.element;
    	return e;
    }
    
    public Node darActualNode(){
    	return actual;
    }
    
    public void setActual(Node n)
    {
    	actual = n;
    }
    
    public Node get(int i)
    {
    	Node current = head;
    	int count = 0;
    	
    	while(current != null)
    	{
    		if(count == i)
    		{
    			return current;
    		}
    		count++;
    		current  = current.next;
    	}
    	return null;
    }
    
    public E getE(int i)
    {
    	Node current = head;
    	
    	while(current != null)
    	{
    		if(current.data == i)
    		{
    			return (E) current.element;
    		}
    		current  = current.next;
    	}
    	return null;
    }
    
    public void swich(int i, int j)
    {
    	if(i == j)
    	{
    		return;
    	}
    	else
    	{
    		E elementoI = (E) get(i).element;
    		E elementoJ = (E) get(j).element;
    		
    		int dataI = get(i).data;
    		int dataJ = get(j).data;
    		
    		get(i).element = elementoJ;
    		get(i).data = dataJ;
    		
    		get(j).element = elementoI;
    		get(j).data = dataI;
    	}
    }
    
    public E getHead( )
    {
    	E e = (E) head.element;
    	return e;
    }
    
    public E getTail( )
    {
    	E e = (E) tail.element;
    	return e;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> it = new Iterator<E>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size && get(currentIndex) != null;
            }

            @Override
            public E next() {
                return (E) get(currentIndex++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }
    
    

	@Override
	public void forEachRemaining(Consumer<? super E> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public E next() {
		// TODO Auto-generated method stub
		return (E) actual.next.element;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
	
	public Node split(Node head) {
        Node fast = head, slow = head;
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        Node temp = slow.next;
        slow.next = null;
        return temp;
    }
 
    public Node mergeSort(Node node) {
    	if (node == null || node.next == null) {
            return node;
        }
        Node second = split(node);
 
        // Recur for left and right halves
        node = mergeSort(node);
        second = mergeSort(second);
        //System.out.println(""+this.realSize());
        // Merge the two sorted halves
        return merge(node, second); 
    }
 
    // Function to merge two linked lists
    public Node merge(Node first, Node second) {
    	if (first == null) {
            return second;
        }
 
        // If second linked list is empty
        if (second == null) {
            return first;
        }
 
        // Pick the smaller value
        if (first.data < second.data) {
            first.next = merge(first.next, second);
            first.next.prev = first;
            first.prev = null;
            return first;
        } else {
            second.next = merge(first, second.next);
            second.next.prev = second;
            second.prev = null;
            return second;
        }
    }
	
	
	public void sort( )
	{
		int sizeR = this.realSize();
		int mitad = sizeR/2;
		int i = 0;
		while(mitad > i)
		{
			int tempMid = mitad;
			while(tempMid < sizeR)
			{
				if(this.get(tempMid).data < this.get(i).data)
				{
					this.swich(tempMid, i);
				}
				tempMid++;
				i++;
			}
			i = 0;
			mitad /= 2;
		}
	}
	
	public void sortBack( )
	{
		int sizeR = this.realSize();
		int mitad = sizeR/2;
		int i = 0;
		while(mitad > i)
		{
			int tempMid = mitad;
			while(tempMid < sizeR)
			{
				if(this.get(tempMid).data > this.get(i).data)
				{
					this.swich(tempMid, i);
				}
				tempMid++;
				i++;
			}
			i = 0;
			mitad /= 2;
		}
	}
}
