package model.data_structures;

import java.util.Iterator;

public class TablaHash<K,V> //LinearProbing
{
	private static final int tamano = 5;
	
	private int size;
	private int objetosEnLista;
	public K[] keys;
	private V[] values;
	
	public TablaHash( )
	{
		this(tamano);
	}
	
	public TablaHash(int pTamano)
	{
		size = pTamano;
		objetosEnLista = 0;
		K[] ks = (K[]) new Object[pTamano];
		keys = ks;
		V[] vs = (V[]) new Object[pTamano];
		values = vs;
	}
	
	public int size( )
	{
		return objetosEnLista;
	}
	
	public boolean isEmpty( )
	{
		return objetosEnLista == 0;
	}
	
	public int hash(K key)
	{
		return (key.hashCode() & 0x7fffffff) % size;
	}
	
	public V get(K key)
	{
		for(int i = hash(key); keys[i] != null; i = (i + 1) % size)
		{
			if(keys[i].equals(key))
			{
				return values[i];
			}
		}
		return null;
	}
	
	public boolean contains(K key)
	{
		return get(key) != null;
	}
	
	public void resize(int pTamano2)
	{
		TablaHash<K,V> temp = new TablaHash<>(pTamano2);
		for(int i = 0; i < size; i++)
		{
			if(keys[i] != null)
			{
				temp.put(keys[i], values[i]);
			}
		}
		
		keys = temp.keys;
		values = temp.values;
		size = temp.size;
	}
	
	public void put(K key,V value)
	{
		if(objetosEnLista >= size/2)
		{
			resize(size*2);
		}
		
		int i = 0;
		for(i = hash(key); keys[i] != null; i = (i + 1) % size)
		{
			if(keys[i].equals(key))
			{
				values[i] = value;
				return;
			}
		}
		
		keys[i] = key;
		values[i] = value;
		objetosEnLista++;
	}
	
	public void remove(K key)
	{
		if(!contains(key))
		{
			return;
		}
		
		int i = hash(key);
		while(!keys[i].equals(key))
		{
			i = (i + 1) % size;
		}
		
		keys[i] = null;
		values[i] = null;
		
		i = (i + 1) % size;
		while(keys[i] != null)
		{
			K tempK = keys[i];
			V tempV = values[i];
			keys[i] = null;
			values[i] = null;
			objetosEnLista--;
			put(tempK, tempV);
			i = (i + 1) % size;
		}
		
		objetosEnLista--;
		
		if(objetosEnLista > 0 && objetosEnLista <= size/8)
		{
			resize(size/2);
		}
	}
	
	public void sortKeys( )
	{
		
	}
	
	/**
     * Returns all keys in this symbol table as an {@code Iterable}.
     * To iterate over all of the keys in the symbol table named {@code st},
     * use the foreach notation: {@code for (Key key : st.keys())}.
     *
     * @return all keys in this symbol table
     */
    public Iterable<K> keys() {
        Queue<K> queue = new Queue<K>();
        for (int i = 0; i < size; i++)
            if (keys[i] != null) queue.enqueue(keys[i]);
        return queue;
    }

}
