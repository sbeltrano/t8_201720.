package model.data_structures;

import java.util.Iterator;

import model.data_structures.DoubleLinkedList.Node;
import model.vo.TripVO;

public interface IDoubleLinkedList<E> {

	public int size();
	
	public void addFirst(E element, int d);
	
	public void addLast(E element, int d);
	
	public void iterateForward();
	public void iterateBackward();
	
	public E removeFirst();
	
	public E removeLast();
	
	public boolean hasNext( );
	
	public boolean hasNext2( );
	public void iteratePreviousOne();
	public E darActual( );
	
	public void iterateOne ( );

	Iterator<E> iterator();

	public void ponerActualPrimero();
	
	public Node mergeSort(Node node);
	
	public Node darActualNode();

	public Node get(int i);

	public E getHead();
	public E getTail();
	
	public int realSize();

	public E getE(int tripID);

	public void sort();
	
	public void sortBack();
	
}
