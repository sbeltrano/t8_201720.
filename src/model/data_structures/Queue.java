package model.data_structures;

import java.util.Iterator;

import model.exceptions.TripNotFoundException;

public class Queue<E> implements IQueue<E>,Iterable<E> {

	private Node<E> first;    // beginning of queue
    private Node<E> last;     // end of queue
    private int size;               // number of elements on queue
    private Node<E> actual;  //actual

    // helper linked list class
    public static class Node<E> {
        private E item;
        private Node<E> next;
    }

    /**
     * Initializes an empty queue.
     */
    public Queue() {
        first = null;
        actual = null;
        last  = null;
        size = 0;
    }

    /**
     * Returns true if this queue is empty.
     *
     * @return {@code true} if this queue is empty; {@code false} otherwise
     */
    public boolean isEmpty() {
        return first == null;
    }
    
    public E darPrimero() {
    	E e = first.item;
		return e;
		
	}
    
    public E darActual(){
    	E e = actual.item;
    	return e;
    } 
    
    public E darUltimo(){
    	E e = last.item;
    	return e;
    }
    
    public void cambiarASig(){
    	actual = actual.next;
    }

    /**
     * Returns the number of items in this queue.
     *
     * @return the number of items in this queue
     */
    public int size() {
        return size;
    }



	public void enqueue(E item) {
		// TODO Auto-generated method stub
		Node<E> oldlast = last;
        last = new Node<E>();
        last.item = item;
        last.next = null;
        if (isEmpty()) first = last;
        else           oldlast.next = last;
        size++;
	}
	
	public boolean hasNext(){
    	return first.next!=null;
    }

	public E dequeue() throws TripNotFoundException {
		// TODO Auto-generated method stub
		if (isEmpty()) throw new TripNotFoundException() ;
        E e = first.item;
        first = first.next;
        size--;
        if (isEmpty()) last = null;   // to avoid loitering
        return e;	}
	
	public Iterator<E> iterator()  {
        return new ListIterator<E>(first);  
    }
	

    // an iterator, doesn't implement remove() since it's optional
    public class ListIterator<E> implements Iterator<E> {
        private Node<E> current;

        public ListIterator(Node<E> first) {
            current = first;
        }

        public boolean hasNext()  { return current != null;                     }
        public void remove()      {   }

        public E next() {
            if (!hasNext()) return null;
            E item = current.item;
            current = current.next; 
            return item;
        }
    }

}