package model.data_structures;

import java.util.ArrayList;

import model.data_structures.DiGraph.Edge;
import model.data_structures.DiGraph.Vertex;

public class CC {
    private boolean[] marked;   // marked[v] = has vertex v been marked?
    private int[] id;           // id[v] = id of connected component containing v
    private int[] size;         // size[id] = number of vertices in given component
    private int count;          // number of connected components
    //private ArrayList<Vertex> vertices;
    private ArrayList<Edge> edges;
    private ArrayList<Vertex> verticesConectados;

    /**
     * Computes the connected components of the undirected graph {@code G}.
     *
     * @param G the undirected graph
     */
    public CC(DiGraph G) {
    	int max = G.maxK();
        marked = new boolean[max+1];
        id = new int[max+1];
        size = new int[G.getSize()];
        //vertices = G.darVertices();
        edges = G.darEdges();
        verticesConectados = new ArrayList<>();
        int i = 0;
        for(Edge e : edges)
        {
        	int a = (int) e.from.value;
        	int b = (int) e.to.value;
        	id[a] = i;
        	id[b] = i;
        	marked[a] = false;
        	marked[b] = false;
        	verticesConectados.add(e.from);
        	verticesConectados.add(e.to);
        	i++;
        }
        
        ArrayList<Vertex> ya = new ArrayList<>();
        for(Vertex ver : verticesConectados)
        {
        	if(!ya.contains(ver))
        	{
        		ya.add(ver);
        		count++;
        	}
        }
    }


    /**
     * Returns the component id of the connected component containing vertex {@code v}.
     *
     * @param  v the vertex
     * @return the component id of the connected component containing vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int id(int v) {
        validateVertex(v);
        return id[v];
    }

    /**
     * Returns the number of vertices in the connected component containing vertex {@code v}.
     *
     * @param  v the vertex
     * @return the number of vertices in the connected component containing vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int size(int v) {
        validateVertex(v);
        return size[id[v]];
    }

    /**
     * Returns the number of connected components in the graph {@code G}.
     *
     * @return the number of connected components in the graph {@code G}
     */
    public int count() {
        return count;
    }

    /**
     * Returns true if vertices {@code v} and {@code w} are in the same
     * connected component.
     *
     * @param  v one vertex
     * @param  w the other vertex
     * @return {@code true} if vertices {@code v} and {@code w} are in the same
     *         connected component; {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     * @throws IllegalArgumentException unless {@code 0 <= w < V}
     */
    public boolean connected(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        return id(v) == id(w);
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = marked.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }


}