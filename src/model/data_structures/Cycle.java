package model.data_structures;

import java.util.ArrayList;

import model.data_structures.DiGraph.Edge;
import model.data_structures.DiGraph.Vertex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import model.data_structures.DiGraph.State;
import model.data_structures.DiGraph.Vertex;

public class Cycle <K extends Comparable<K>, T extends Comparable<T>> {

	private Stack<Vertex> cycle;
	private ArrayList<DiGraph<K, T>.Vertex> vertices;
	//private ArrayList<Stack<DiGraph<K, T>.Vertex>> listaCiclos;



	public Cycle(DiGraph<K, T> G)
	{
		
		vertices = G.darVertices(); 
		
		clearStates();

		for (Vertex vertex : vertices) {

			if(vertex.state == State.UNVISITED && cycle == null)
			{
				DepthFirstSearch(vertex);
			}
		}
		
		
	}



	private void DepthFirstSearch(Vertex v)
	{
		v.state = State.VISITED;
		v.enStack = true;


		List<Vertex> lista = v.outgoing;



		//loop through neighbors
		for (Vertex w : lista)
		{

			if (cycle != null) return;

			else if (w.state ==State.UNVISITED)
			{
				w.previous = v; 
				DepthFirstSearch(w);
			}
			else if(w.enStack == true)
			{
				cycle = new Stack<>();

				for (Vertex x = v; x != w;  x = x.previous) 
				{
					cycle.push(x);
				}
				cycle.push(w);
				cycle.push(v); 
			}
		}
		
		
		
		v.state = State.VISITED;
		v.enStack = false;

	}




	private void clearStates()
	{
		for (Vertex each : vertices)
		{
			each.state = State.UNVISITED;
			each.enStack = false;
		}
	}

	
	public boolean hayCiclo()
	{
		return cycle != null;
	}


	public Stack<Vertex> darCiclo()
	{
		return cycle; 
	}
	
}
