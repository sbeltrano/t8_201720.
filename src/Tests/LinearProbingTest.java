package Tests;

import org.junit.Test;
import model.data_structures.TablaHash;
import static org.junit.Assert.*;

import model.data_structures.Queue;
import model.exceptions.TripNotFoundException;

public class LinearProbingTest {
	
	private TablaHash<Integer,Integer> tabla;
	
	public void scenario1( )
	{
		tabla.put(0, 0);
		tabla.put(1, 1);
		tabla.put(2, 2);
		tabla.put(3, 3);
		tabla.put(4, 4);
		tabla.put(5, 5);
	}
	
	public void testPut( )
	{
		tabla = new TablaHash<>();
		scenario1();
		assertTrue(tabla.size() == 6);
	}
	
	public void testRemove( )
	{
		tabla = new TablaHash<>();
		scenario1();
		tabla.remove(4);
		assertTrue(tabla.get(4) == null);
	}
	
	public void testGet( )
	{
		tabla = new TablaHash<>();
		scenario1();
		assertTrue(tabla.get(5) == 5);
	}
	
	@Test
	public void test( )
	{
		testPut();
		testRemove();
		testGet();
	}
}
