package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.MaxPQ;
import model.vo.StopVO;

public class MaxPQTest {
	private MaxPQ<Integer> rbBST;
	
	private StopVO stop1;
	private StopVO stop2;
	private StopVO stop3;
	private StopVO stop4;
	
	public void scenario1( )
	{
		rbBST = new MaxPQ<Integer>();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		rbBST.insert(stop1.id());
		rbBST.insert(stop2.id());
		rbBST.insert(stop3.id());
		rbBST.insert(stop4.id());
	}
	
	
	public void deletemMax(){
		scenario1();
		rbBST.delMax();
		assertTrue(rbBST.max().equals(stop4.id()));
	}
	public void insertTest(){
		scenario1();
		assertTrue(rbBST.size()>3);
	}
	@Test
	public void test() {
		insertTest();
		deletemMax();
	}


}
