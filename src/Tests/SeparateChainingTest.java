package Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import model.data_structures.SeparateChainingHashST;

public class SeparateChainingTest {
	
private SeparateChainingHashST<Integer,Integer> tabla;
	
	public void scenario1( )
	{
		tabla.put(0, 0);
		tabla.put(1, 1);
		tabla.put(2, 2);
		tabla.put(3, 3);
		tabla.put(4, 4);
		tabla.put(5, 5);
	}
	
	public void testPut( )
	{
		tabla = new SeparateChainingHashST<>();
		scenario1();
		assertTrue(tabla.size() == 6);
	}
	
	public void testRemove( )
	{
		tabla = new SeparateChainingHashST<>();
		scenario1();
		tabla.delete(4);
		assertTrue(tabla.get(4) == null);
	}
	
	public void testGet( )
	{
		tabla = new SeparateChainingHashST<>();
		scenario1();
		assertTrue(tabla.get(5) == 5);
	}
	
	@Test
	public void test( )
	{
		testPut();
		testRemove();
		testGet();
	}
}
