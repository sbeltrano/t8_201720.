package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;
import model.exceptions.TripNotFoundException;
import model.vo.BusUpdateVO;
import model.vo.RouteMapVO;

public class StackTest {

	private Stack<BusUpdateVO> stack;
	private BusUpdateVO busUpdate1;
	private BusUpdateVO busUpdate2;
	private BusUpdateVO busUpdate3;
	private BusUpdateVO busUpdate4;
	private RouteMapVO route;
	
	public void scenario1( )
	{
		stack = new Stack();
		route = new RouteMapVO("");
		busUpdate1 = new BusUpdateVO("", 1, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate2 = new BusUpdateVO("", 2, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate3 = new BusUpdateVO("", 3, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate4 = new BusUpdateVO("", 3, "",
				"", "", "",
				1, 2, "",
				route);
		
		stack.push(busUpdate1);
		stack.push(busUpdate2);
		stack.push(busUpdate3);
		stack.push(busUpdate4);
	}
	public void scenario2( )
	{
		stack = new Stack();
		route = new RouteMapVO("");
		busUpdate1 = new BusUpdateVO("", 1, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate2 = new BusUpdateVO("", 2, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate3 = new BusUpdateVO("", 3, "",
				"", "", "",
				1, 2, "",
				route);
		busUpdate4 = new BusUpdateVO("", 3, "",
				"", "", "",
				1, 2, "",
				route);
		
		stack.push(busUpdate1);
		stack.push(busUpdate2);
		stack.push(busUpdate3);
		stack.push(busUpdate4);
		stack.pop();
	}
	
	public void testAgregar(){
		scenario1();
		assertTrue(stack.size()==4);
	}
	public void testEliminar(){
		scenario2();
		assertTrue(stack.size()==3);
	}
	@Test
	public void test() {
		testAgregar();
		testEliminar();
	}

}
