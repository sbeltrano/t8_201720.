package Tests;

import static org.junit.Assert.*;
import model.data_structures.MinPQ;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;
import org.junit.Test;

public class MinPQTest {
	
	private MinPQ<Integer> rbBST;
	
	private StopVO stop1;
	private StopVO stop2;
	private StopVO stop3;
	private StopVO stop4;
	
	public void scenario1( )
	{
		rbBST = new MinPQ<Integer>();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		rbBST.insert(stop1.id());
		rbBST.insert(stop2.id());
		rbBST.insert(stop3.id());
		rbBST.insert(stop4.id());
	}
	
	
	public void deletemMin(){
		scenario1();
		rbBST.delMin();
		assertTrue(rbBST.min().equals(stop2.id()));
	}
	public void insertTest(){
		scenario1();
		assertTrue(rbBST.size()>3);
	}
	@Test
	public void test() {
		insertTest();
		deletemMin();
	}

}
