package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.RedBlackBST;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;

public class RedBlackBSTTest {
	
	private RedBlackBST<Integer,StopVO> rbBST;
	
	private StopVO stop1;
	private StopVO stop2;
	private StopVO stop3;
	private StopVO stop4;
	
	public void scenario1( )
	{
		rbBST = new RedBlackBST();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		rbBST.put(stop1.id(), stop1);
		rbBST.put(stop2.id(), stop2);
		rbBST.put(stop3.id(), stop3);
		rbBST.put(stop4.id(), stop4);
	}
	
	public void scenario2( )
	{
		rbBST = new RedBlackBST();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		rbBST.put(stop1.id(), stop1);
		rbBST.put(stop2.id(), stop2);
		rbBST.put(stop3.id(), stop3);
		rbBST.put(stop4.id(), stop4);

		rbBST.delete(1);

	}
	
	public void testGet(){
		scenario1();
		//System.out.println(rbBST.get(1).id());
		assertTrue("Agrego el elemento", rbBST.get(1).id()==(1));
		assertTrue("Agrego el elemento", rbBST.get(2).equals(stop2));
		assertTrue("Agrego el elemento", rbBST.get(3).equals(stop3));
		assertTrue("Agrego el elemento", rbBST.get(4).equals(stop4));

	}
	
	public void testPut(){
		scenario1();
		assertTrue(rbBST.size()>3);
	}
	
	public void testDelete(){
		scenario2();
		assertNull(rbBST.get(1));
	}
	
	public void testDeleteMin(){
		scenario1();
		rbBST.deleteMin();
		assertNull(rbBST.get(1));

	}
	
	public void testDeleteMax(){
		scenario1();
		rbBST.deleteMax();
		assertNull(rbBST.get(4));

	}
	
	@Test
	public void test() {
		testGet();
		testDelete();
		testPut();
		testDeleteMax();
		testDeleteMin();
	}

}
