package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.CC;
import model.data_structures.DiGraph;
import model.vo.StopVO;

public class CCtest {
	DiGraph graph = new DiGraph();
	DiGraph graph1 = new DiGraph(); 
	CC cc1;
	CC cc2;
	
	public void Scenario1(){
		// add a bunch of edges
		StopVO stop1 = new StopVO(1,"","","ruta1","","","","","","");
		StopVO stop2 = new StopVO(2,"","","ruta2","","","","","","");
		StopVO stop3 = new StopVO(3,"","","ruta3","","","","","","");
		StopVO stop4 = new StopVO(4,"","","ruta4","","","","","","");
		graph.addVertex(stop1.id(), stop1);
		graph.addVertex(stop2.id(), stop2);
		graph.addVertex(stop3.id(), stop3);
		graph.addVertex(stop4.id(), stop4);
		
		graph.addEdge(stop1.id(), stop2.id(), 5);
		graph.addEdge(stop1.id(), stop3.id(), 10);
		graph.addEdge(stop1.id(), stop4.id(), 15);
		graph.addEdge(stop2.id(), stop3.id(), 5);

		cc1 = new CC(graph);
		System.out.print("All edges: "+graph.edgesToString());
	}
	public void Scenario2(){
		// add a bunch of edges
		StopVO stop1 = new StopVO(1,"","","ruta1","","","","","","");
		StopVO stop2 = new StopVO(2,"","","ruta2","","","","","","");
		StopVO stop3 = new StopVO(3,"","","ruta3","","","","","","");
		StopVO stop4 = new StopVO(4,"","","ruta4","","","","","","");
		graph1.addVertex(stop1.id(), stop1);
		graph1.addVertex(stop2.id(), stop2);
		graph1.addVertex(stop3.id(), stop3);
		graph1.addVertex(stop4.id(), stop4);
		
		graph1.addEdge(stop1.id(), stop4.id(), 15);
		graph1.addEdge(stop1.id(), stop2.id(), 5);
		graph1.addEdge(stop1.id(), stop3.id(), 10);
		graph1.addEdge(stop2.id(), stop3.id(), 5);

		cc2 = new CC(graph1);
		System.out.print("All edges: "+graph1.edgesToString());
	}
	
	public void connectedComponentsTest(){
		Scenario1();
		System.out.println("Numero de componentes conectados: "+cc1.count());
		assertTrue(cc1.count()==4);
	}
	
	public void connectedComponentsTest2(){
		Scenario2();
		System.out.println("Numero de componentes conectados: "+cc2.count());
		assertTrue(cc2.count()==4);
	}
	
	@Test
	public void test() {
		connectedComponentsTest2();
		connectedComponentsTest();
	}

}
