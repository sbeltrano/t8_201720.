package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;
import model.exceptions.TripNotFoundException;
import model.vo.StopVO;

public class QueueTest {

	private Queue<StopVO> cola;
	
	private StopVO stop1;
	private StopVO stop2;
	private StopVO stop3;
	private StopVO stop4;
	
	public void scenario1( )
	{
		cola = new Queue();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		cola.enqueue(stop1);
		cola.enqueue(stop2);
		cola.enqueue(stop3);
		cola.enqueue(stop4);
	}
	
	public void scenario2( )
	{
		cola = new Queue();
		
		stop1 = new StopVO(1,"","","","","","","","","");
		stop2 = new StopVO(2,"","","","","","","","","");
		stop3 = new StopVO(3,"","","","","","","","","");
		stop4 = new StopVO(4,"","","","","","","","","");
		
		cola.enqueue(stop1);
		cola.enqueue(stop2);
		cola.enqueue(stop3);
		cola.enqueue(stop4);
		try {
			cola.dequeue();
		} catch (TripNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testAgregar(){
		scenario1();
		assertTrue(cola.size()==4);
	}
	public void testEliminar(){
		scenario2();
		assertTrue(cola.size()==3);
	}
	@Test
	public void test() {
		testAgregar();
		testEliminar();
	}

}
