package api;

import java.io.File;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.RedBlackBST;
import model.data_structures.TablaHash;
import model.exceptions.TripNotFoundException;
import model.vo.AgencyVO;
import model.vo.StopVO;
import model.vo.TripVO;
import model.vo.VOPlan;
import model.vo.VORangoHora;
import model.vo.VORetardo;
import model.vo.VORoute;
import model.vo.VOStopTimes;
import model.vo.VOTransfer;

public interface ISTSMAnager {
	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	
	public void buscarLat();
	
	public void readBusUpdate(File rtFile);
	
	public void loadStops(String file);
	
	public void readStopsEstimService(File rtFile);
	
	public void loadAgencies(String agencyFile);
	
	public void loadTrips(String tripFile);
	
	public void loadCalendar(String tripFile);
	
	public void loadCalendarDates(String tripFile);
	
	public void loadRoutes(String tripFile);
	
	public void loadShapes(String tripFile);
	
	public void loadFeedInfo(String tripFile);
	
	public void loadStopTimes(String tripFile);
	
	public void loadTransfers(String tripFile);

	void cargarRetardosViaje();
	
	public void agregarAGrafo();
	
	public void cuantosConectados();
	
	public void tieneCiclos();


}
